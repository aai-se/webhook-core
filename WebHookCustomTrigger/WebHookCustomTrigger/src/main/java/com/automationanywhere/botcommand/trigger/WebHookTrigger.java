/*
 * Copyright (c) 2020 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */

package com.automationanywhere.botcommand.trigger;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.RecordValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.record.Record;
import com.automationanywhere.commandsdk.annotations.*;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;

import static com.automationanywhere.commandsdk.model.DataType.RECORD;

/**
 * 
 * This is a Custom Trigger class used to get triggered on receiving a request on the specified Url.
 * Here, we have used it to trigger on receiving webhook request from the webhook receiver (via SignalR).
 *
 */
@BotCommand(commandType = BotCommand.CommandType.Trigger)
@CommandPkg(label = "WebHook Trigger", description = "WebHook Trigger", icon = "jms.svg", name = "WebhookTrigger",
		return_type = RECORD, return_name = "TriggerData", return_description = "Available keys: triggerType")
public class WebHookTrigger {

	// Map storing multiple HubConnection instance.
	private static final Map<String, HubConnection> taskMap = new ConcurrentHashMap<>();

	@TriggerId
	private String triggerUid;
	@TriggerConsumer
	private Consumer consumer;
	
	//This method is called by HubConnection when a message arrives to store the data recieved from webhook source.
	private RecordValue getRecordValue(String data) {
		List<Schema> schemas = new LinkedList<>();
		List<Value> values = new LinkedList<>();
		schemas.add(new Schema("triggerType"));
		values.add(new StringValue(data));

		RecordValue recordValue = new RecordValue();
		recordValue.set(new Record(schemas,values));
		return recordValue;
	}
	/*
	 * Starts the trigger.
	 * 
	 * We will use this method to setup the trigger, i.e. setup the HubConnection and start it.
	 */
	@StartListen
	public void startTrigger(@Idx(index = "1", type = AttributeType.TEXT)
	@Pkg(label = "Please provide the broker URL")
	@NotEmpty
	String webhookreceiverURL
							 /*@Idx(index = "2", type = AttributeType.TEXT)
	@Pkg(label = "Please provide the queue name")
	@NotEmpty
	String queueName*/) {
		
		if (taskMap.get(triggerUid) == null) {
			synchronized (this) {
				if (taskMap.get(triggerUid) == null) {

					//String input = "https://webhookcorereceiver.azurewebsites.net/github";
					HubConnection hubConnection = HubConnectionBuilder.create(webhookreceiverURL)
							.build();

					//Bind the Hub (server) Connected event with client method.
					hubConnection.on("OnHubConnected", (message) -> {
						//Write the things to do on connect to the Hub (server).
					}, String.class);

					//Bind the Hub (server) On Message received event with client method.
					hubConnection.on("OnMessageReceived", (id, message) -> {
						//Pass the data (message) from Hub (server) to this consumer.
						this.consumer.accept(getRecordValue(message));
					}, String.class, String.class);

					//Add the HubConnection instance to map the trigger task.
					taskMap.put(triggerUid, hubConnection);

					//Initiate the WebSocket connection to the Hub (server).
					hubConnection.start().blockingAwait();
				}
			}
		}

	}

	/*
	 * Cancel all the task and clear the map.
	 */
	@StopAllTriggers
	public void stopAllTriggers() {
		taskMap.forEach((k, v) -> {
			v.stop();
			taskMap.remove(k);
		});
	}

	/*
	 * Cancel the task and remove from map
	 *
	 * @param triggerUid
	 */
	@StopListen
	public void stopListen(String triggerUid) {
		taskMap.get(triggerUid).stop();
		taskMap.remove(triggerUid);
	}

	public String getTriggerUid() {
		return triggerUid;
	}

	public void setTriggerUid(String triggerUid) {
		this.triggerUid = triggerUid;
	}

	public Consumer getConsumer() {
		return consumer;
	}

	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
}
