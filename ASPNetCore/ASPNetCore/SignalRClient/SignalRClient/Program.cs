﻿using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace SignalRCoreClient
{
    class Program
    {
        static readonly string baseUrl = "https://webhookcorereceiver.azurewebsites.net";
        static async Task Main(string[] args)
        {
            HubConnection hubConnection;
            hubConnection = new HubConnectionBuilder()
                //.WithUrl("https://localhost:5001/github")
                .WithUrl(baseUrl)// + "/github")
                .Build();

            hubConnection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await hubConnection.StartAsync();
            };

            //Register the client side methods that will be called from the Hub (SignalR Server).
            //Map the client method on Connection with Hub.
            hubConnection.On<string>("OnHubConnected", (message) => Console.WriteLine("Msg from Hub Server: " + message));

            //Map the client method on receiving the message from the Hub server.
            hubConnection.On<string, string>("OnMessageReceived", MessageReceived);
            
            //Map the client method on Disconnect with Hub.
            hubConnection.On<string>("OnHubDisconnected", (message) => Console.WriteLine("Msg from Hub Server: " + message));

            //Start the Connection with the Hub (SignalR Server).
            await hubConnection.StartAsync();

            Console.WriteLine("Connected!");
            Console.ReadLine();

            await hubConnection.StopAsync();

            //Call to Server SendMessage() method.
            //await hubConnection.InvokeAsync("SendMessage",
            //        sender, messageTextBox.Text);

            //while (true)
            //{
            //    var message = Console.ReadLine();
            //    await hubConnection.SendAsync("SendMessage", message);
            //}
        }

        static void MessageReceived(string generator, string data)
        {
            Console.WriteLine("Data from Hub Server: \nGenerator: " + generator + "\nData: " + data);
        }
    }
}
