// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.

using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalR.Hubs
{
    public class WebHookReceiverHub : Hub
    {
        public override Task OnConnectedAsync()
        {
            //var name = Context.GetHttpContext().Request.Query["name"];
            return Clients.All.SendAsync("OnHubConnected", "Connected to Hub Server...");
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            //var name = Context.GetHttpContext().Request.Query["name"];
            return Clients.All.SendAsync("OnHubDisconnected", "Disconnected from Hub Server.");
        }

        //public Task Send(string name, string message)
        //{
        //    return Clients.All.SendAsync("ReceiveMessage", name, message + " Message from Hub Send() method.");
        //}

    }
}
