using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.WebHooks;
using Newtonsoft.Json.Linq;
using SignalR.Hubs;

namespace WebHookCoreReceiver.Controllers
{
    public class GitHubWebHookController : ControllerBase
    {
        readonly IHubContext<WebHookReceiverHub> hubContext;

        //Use Dependency injection of SignalR Hub Context to use outside of Hub class. Pass the Hub Context in the Controller.
        public GitHubWebHookController(IHubContext<WebHookReceiverHub> hubContext)
        {
            this.hubContext = hubContext;
        }

        [GitHubWebHook(EventName = "push", Id = "It")]
        public IActionResult HandlerForItsPushes(string[] events, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [GitHubWebHook(Id = "It")]
        public IActionResult HandlerForIt(string[] events, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [GitHubWebHook(EventName = "push")]
        public IActionResult HandlerForPush(string id, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [GitHubWebHook]
        public IActionResult GitHubHandler(string sender, string @event, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Call the Client side method with JSON data passed as a parameters.
            sender = "Github";
            hubContext.Clients.All.SendAsync("OnMessageReceived", sender, data.ToString());

            return Ok("Successfully received " + sender + " payload to the Receiver");
        }        

        [GeneralWebHook]
        public IActionResult FallbackHandler(string receiverName, string id, string eventName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok("Fallback Handler: " + receiverName);
        }
    }
}
