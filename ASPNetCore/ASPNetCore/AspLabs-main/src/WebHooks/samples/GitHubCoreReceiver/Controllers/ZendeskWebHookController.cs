using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.WebHooks;
using Newtonsoft.Json.Linq;
using SignalR.Hubs;

namespace WebHookCoreReceiver.Controllers
{
    public class ZendeskWebHookController : ControllerBase
    {
        readonly IHubContext<WebHookReceiverHub> hubContext;

        //Use Dependency injection of SignalR Hub Context to use outside of Hub class. Pass the Hub Context in the Controller.
        public ZendeskWebHookController(IHubContext<WebHookReceiverHub> hubContext)
        {
            this.hubContext = hubContext;
        }

        [ZendeskWebHook(EventName = "push", Id = "It")]
        public IActionResult HandlerForItsPushes(string[] events, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [ZendeskWebHook(Id = "It")]
        public IActionResult HandlerForIt(string[] events, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [ZendeskWebHook(EventName = "push")]
        public IActionResult HandlerForPush(string id, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [ZendeskWebHook]
        public IActionResult ZendeskHandler(string sender, string @event, JObject data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Call the Client side method with JSON data passed as a parameters.
            sender = "Zendesk";
            hubContext.Clients.All.SendAsync("OnMessageReceived", sender, data.ToString());

            return Ok("Successfully received " + sender + " payload to the Receiver");
        }

        [GeneralWebHook]
        public IActionResult FallbackHandler(string receiverName, string id, string eventName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok("Fallback Handler: " + receiverName);
        }
    }
}
