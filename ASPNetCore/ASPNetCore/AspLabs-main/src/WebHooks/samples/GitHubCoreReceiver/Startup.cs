using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using SignalR.ConnectionHandlers;
using SignalR.Hubs;

namespace WebHookCoreReceiver
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //Removed the call to UseMvc() that was supported in ASP.Net Core v2.1.
            //services
            //    .AddMvcCore()
            //    .AddGitHubWebHooks();

            //services.AddMvc().AddGitHubWebHooks();
            services.AddMvc().AddNewtonsoftJson();

            //Add Github extensions to the Controller...
            services.AddControllersWithViews().AddGitHubWebHooks();
            services.AddRazorPages().AddGitHubWebHooks();

            //Add Zendesk extensions to the Controller...
            //services.AddControllersWithViews().AddZendeskWebHooks();
            //services.AddRazorPages().AddZendeskWebHooks();

            //services.AddConnections();
            services.AddSignalR().AddJsonProtocol();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseMvc();  //Removed the call to UseMvc() that was supported in ASP.Net Core v2.1.

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();

                endpoints.MapHub<WebHookReceiverHub>("/github");
                //endpoints.MapConnectionHandler<MessagesConnectionHandler>("/github");

                //endpoints.MapHub<WebHookReceiverHub>("/zendesk");
            });
        }
    }
}
