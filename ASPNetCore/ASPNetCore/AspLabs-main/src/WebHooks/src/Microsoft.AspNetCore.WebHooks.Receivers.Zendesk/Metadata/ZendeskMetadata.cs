// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using Microsoft.AspNetCore.WebHooks.Filters;

namespace Microsoft.AspNetCore.WebHooks.Metadata
{
    /// <summary>
    /// An <see cref="IWebHookMetadata"/> service containing metadata about the Dropbox receiver.
    /// </summary>
    public class ZendeskMetadata :
        WebHookMetadata,
        IWebHookEventMetadata,
        IWebHookFilterMetadata,
        IWebHookGetHeadRequestMetadata
    {
        private readonly ZendeskVerifySignatureFilter _verifySignatureFilter;

        /// <summary>
        /// Instantiates a new <see cref="ZendeskMetadata"/> instance.
        /// </summary>
        /// <param name="verifySignatureFilter">The <see cref="ZendeskVerifySignatureFilter"/>.</param>
        public ZendeskMetadata(ZendeskVerifySignatureFilter verifySignatureFilter)
            : base(ZendeskConstants.ReceiverName)
        {
            _verifySignatureFilter = verifySignatureFilter;
        }

        // IWebHookBodyTypeMetadataService...

        /// <inheritdoc />
        public override WebHookBodyType BodyType => WebHookBodyType.Json;

        // IWebHookEventMetadata...

        /// <inheritdoc />
        public string ConstantValue => ZendeskConstants.EventName;

        /// <inheritdoc />
        public string HeaderName => null;

        /// <inheritdoc />
        public string QueryParameterName => null;

        // IWebHookGetHeadRequestMetadata...

        /// <inheritdoc />
        public bool AllowHeadRequests => false;

        /// <inheritdoc />
        public string ChallengeQueryParameterName => ZendeskConstants.ChallengeQueryParameterName;

        /// <inheritdoc />
        public int SecretKeyMinLength => ZendeskConstants.SecretKeyMinLength;

        // IWebHookFilterMetadata...

        /// <inheritdoc />
        public void AddFilters(WebHookFilterMetadataContext context)
        {
            context.Results.Add(_verifySignatureFilter);
        }
    }
}
