import java.util.Scanner;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

public class WebHookSignalRClient {
    public static void main(String[] args) {
        //System.out.println("Enter the URL of the SignalR Hub to connect with: ");
        //Scanner reader = new Scanner(System.in);
        //String url = "https://webhookcorereceiver.azurewebsites.net/github";
        //url = reader.nextLine();

        String input = "https://webhookcorereceiver.azurewebsites.net/zendesk";
        HubConnection hubConnection = HubConnectionBuilder.create(input)
                .build();

        hubConnection.on("OnHubConnected", (message) -> {
            System.out.println("Message from Hub: " + message);
        }, String.class);

        hubConnection.on("OnMessageReceived", (sender, message) -> {
            System.out.println("Data from Hub: " + message + "by the sender: " + sender);
        }, String.class, String.class);

        //Initiate the WebSocket connection to the Hub (server).
        hubConnection.start().blockingAwait();

        Scanner reader = new Scanner(System.in);
        reader.nextLine();

        hubConnection.stop();

        /*
        while (!input.equals("leave")){
            input = reader.nextLine();
            hubConnection.send("Send", input);
        }
        */
    }
}
